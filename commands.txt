docker-compose up -d
docker ps
docker-compose exec kafka sh 
docker-compose exec zookeeper sh 

// criando um topico
kafka-topics --create --topic meu-topico-legal --partitions 1 --replication-factor 1 --if-not-exists --zookeeper localhost:32181

// verificando se o topico está criado
kafka-topics --describe --topic meu-topico-legal --zookeeper localhost:32181

// produzindo mensagens com o producer
bash -c "seq 100 | kafka-console-producer --request-required-acks 1 --broker-list localhost:29092 --topic meu-topico-legal && echo 'Produced 100 messages.'"

//consumindo mensagens com o comsumer
kafka-console-consumer --bootstrap-server localhost:29092 --topic meu-topico-legal --from-beginning --max-messages 100

//criando um topic
kafka-topics --zookeeper 127.0.0.1:32181 --topic fist_topic --create --partitions 10 --replication-factor 1
kafka-topics --describe --topic fist_topic --zookeeper localhost:32181
kafka-topics --zookeeper 127.0.0.1:32181 --topic third_topic --create --partitions 5 --replication-factor 1

//listando os topicos
kafka-topics --zookeeper 127.0.0.1:32181 --list

//delete topico
kafka-topics --zookeeper 127.0.0.1:32181 --topic first_topic --delete

//produzindo para o kafka
kafka-console-producer --broker-list localhost:29092 --topic first_topic

//produzindo para o kafka - --producer-property asks=all
kafka-console-producer --broker-list 127.0.0.1:29092 --topic first_topic --producer-property acks=all

//produzindo para um topico que não existe
kafka-console-producer --broker-list localhost:29092 --topic new_topic
kafka-topics --zookeeper 127.0.0.1:32181 --list

//consumindo só as mensagens que foram produzidas na hora
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic 

// consumindo as mensagens antigas
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic  --from-beginning --max-messages 100

// dois consumidores com o mesmo grupo somente um consumirá com uma partição
Topic: first_topic	PartitionCount: 1	ReplicationFactor: 1 Configs: Topic: first_topic	Partition: 0	Leader: 1	Replicas: 1	Isr: 1
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic --group my-first-application
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic --group my-first-application
kafka-topics --describe --topic first_topic --zookeeper localhost:32181


// dois consumidores diferentes grupos os dois consumirá 
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic --group my-first-application
kafka-console-consumer --bootstrap-server localhost:29092 --topic first_topic --group my-second-application


//listando grupos de consumidores
kafka-consumer-groups --bootstrap-server localhost:29092 --list 

//describe group 
kafka-consumer-groups --bootstrap-server localhost:29092 --describe --group my-first-application

//reset offsets
kafka-consumer-groups --bootstrap-server localhost:29092  --group my-first-application --reset-offsets --to-earliest  --execute --topic first_topic


















